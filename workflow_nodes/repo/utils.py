# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
import sys
from functools import wraps

from kadi_apy.lib.core import KadiAPI
from kadi_apy.lib.exceptions import KadiAPYException
from xmlhelpy import Integer
from xmlhelpy import option
from xmlhelpy import String


def repo_command(func):
    """Decorator to handle the default arguments and exceptions of an APY command."""

    option(
        "host",
        char="h",
        description="Host name of the Kadi4Mat instance to use for the API.",
    )(func)

    option(
        "token",
        char="k",
        description="Personal access token (PAT) to use for the API.",
    )(func)

    option(
        "skip-verify",
        char="s",
        is_flag=True,
        description="Skip verifying the SSL/TLS certificate of the host.",
    )(func)

    @wraps(func)
    def decorated_command(token, host, skip_verify, *args, **kwargs):
        KadiAPI.token = token if token is not None else os.environ.get("KADI_PAT")
        KadiAPI.host = host if host is not None else os.environ.get("KADI_HOST")
        KadiAPI.verify = not skip_verify

        try:
            func(*args, **kwargs)
        except KadiAPYException as e:
            print(e, file=sys.stderr)
            sys.exit(1)

    return decorated_command


def id_identifier_options(item, helptext=None):
    """Decorator to handle the id and identifier."""

    def decorator(func):
        help_id = f"ID of the {item}"
        if helptext:
            help_id = f"{help_id} {helptext}"

        help_identifier = f"Identifier of the {item}"
        if helptext:
            help_identifier = f"{help_identifier} {helptext}"

        option(
            f"{item}-id",
            char=f"{item[0]}",
            description=help_id,
            default=None,
            param_type=Integer,
        )(func)

        option(
            f"{item}-identifier",
            char=f"{item[0].upper()}",
            description=help_identifier,
            default=None,
            param_type=String,
        )(func)

        @wraps(func)
        def decorated_command(*args, **kwargs):
            item_id = kwargs[f"{item}_id"]
            item_identifier = kwargs[f"{item}_identifier"]
            if (item_id is None and item_identifier is None) or (
                item_id is not None and item_identifier is not None
            ):
                print(f"Please specify either the id or the identifier of the {item}.")
                sys.exit(1)
            func(*args, **kwargs)

        return decorated_command

    return decorator
