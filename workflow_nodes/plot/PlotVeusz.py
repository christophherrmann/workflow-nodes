# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
import subprocess

from xmlhelpy import command
from xmlhelpy import option


@command(
    name="PlotVeusz",
    version="0.0.1",
    description="A Veusz-1.21.1 based node for plotting columns from a text file",
)
@option(
    "pathfile",
    char="i",
    required=True,
    description="Name of the input file including import path to the file",
)
@option(
    "plotoutput",
    char="p",
    required=True,
    default=None,
    description="Resulting plot, the format is controlled by file extension:"
    " *.bmp, *.eps, *.jpg, *.pdf, *.png *.svg, *.tiff, *.xpm",
)
@option(
    "columns",
    char="u",
    required=False,
    description="Specified columns from input file to be printed, e.g. -u 2:3",
)
@option("output", char="o", required=False, description="Text file with Veusz commands")
@option(
    "parameter_x",
    char="x",
    required=False,
    default=None,
    description="Parameter for the x axis",
)
@option(
    "parameter_y",
    char="y",
    required=False,
    default=None,
    description="Parameter for the y axis",
)
@option(
    "keylabel",
    char="l",
    required=False,
    default=None,
    description="Key label for the curve",
)
@option(
    "font",
    char="s",
    required=False,
    default="cmr10",
    description="Font for axis label, numbers and key label",
)
def execute(*args, **kwargs):
    """execute"""

    pathfile = kwargs["pathfile"]
    output = kwargs["output"]
    columns = kwargs["columns"]
    parameter_x = kwargs["parameter_x"]
    parameter_y = kwargs["parameter_y"]
    keylabel = kwargs["keylabel"]
    plotoutput = kwargs["plotoutput"]
    font = kwargs["font"]

    path_file = os.path.split(pathfile)

    if output is None:
        output = os.path.splitext(plotoutput)[0]

    # Get column names of the specified columns
    new_linearray = []
    with open(pathfile) as textfile:
        for line in textfile:
            if line.startswith("descriptor"):
                linearray = line.split("  ")
                for entry in linearray:
                    if len(entry) == 0 or entry == "\n":
                        continue
                    new_linearray.append(entry)

    if columns is not None:
        columnname_x = new_linearray[int(columns[0])]
        columnname_x = columnname_x.strip()
        columnname_y = new_linearray[int(columns[2])]
        columnname_y = columnname_y.strip()

    with open(f"{output}.vsz", "w", encoding="utf-8") as f:
        f.write(f"AddImportPath(u'{path_file[0]}')\n")
        f.write(f"ImportFile(u'{path_file[1]}', u'', ignoretext=True, linked=True)\n")
        f.write(
            "Set('StyleSheet/axis/Label/offset', u'4pt')\n"
            "Set('StyleSheet/axis/TickLabels/offset', u'4pt')\n"
        )
        if font != "cmr10":
            f.write(f"Set('StyleSheet/Font/font', u'{font}')\n")

        f.write(
            "Add('page', name='page1', autoadd=False)\n"
            "To('page1')\n"
            "Add('graph', name='graph1', autoadd=False)\n"
            "To('graph1')\n"
            "Add('axis', name='x', autoadd=False)\n"
            "To('x')\n"
        )
        if parameter_x is not None:
            f.write(f"Set('label', u'{parameter_x}')\n")
        else:
            f.write(f"Set('label', u'{columnname_x}')\n")

        f.write("To('..')\n" "Add('axis', name='y', autoadd=False)\n" "To('y')\n")
        if parameter_y is not None:
            f.write(f"Set('label', u'{parameter_y}')\n")
        else:
            f.write(f"Set('label', u'{columnname_y}')\n")

        f.write(
            "Set('direction', 'vertical')\n"
            "To('..')\n"
            "Add('xy', name='xy1', autoadd=False)\n"
            "To('xy1')\n"
        )

        if columns is not None:
            f.write(f"Set('xData', u'{columnname_x}')\n")
        else:
            f.write(f"Set('xData', u'{parameter_x}')\n")

        if columns is not None:
            f.write(f"Set('yData', u'{columnname_y}')\n")
        else:
            f.write(f"Set('yData', u'{parameter_y}')\n")

        if keylabel is not None:
            f.write(f"Set('key', u'{keylabel}')\n")
            f.write(
                "To('..')\n"
                "Add('key', name='key1', autoadd=False)\n"
                "To('key1')\n"
                "Set('title', u'')\n"
                "To('..')\n"
            )
        f.write("To('..')\n" "To('..')\n")

        # Export plot and finish script; Last two commands should be removed,
        # if the Veusz-GUI wants to be used
        if plotoutput is not None:
            f.write(f'Export("{plotoutput}")\n')

        f.write("Quit()\n")

    # Execute veusz without GUI
    subprocess.call(["veusz", "--listen", "--quiet"], stdin=open(f"{output}.vsz"))


if __name__ == "__main__":
    execute()
