#!/usr/bin/python3
# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import subprocess
import sys

from xmlhelpy import argument
from xmlhelpy import command
from xmlhelpy import option


@command(
    name="sed",
    version="4.4",
    description=(
        "Wrapper node for sed to replace a string with another string in a file"
    ),
)
@argument("string1", required=True, description="string to be replaced")
@argument("string2", required=True, description="string to be inserted")
@option("path", char="p", required=True, description="Name of the file")
def execute(*args, **kwargs):
    """execute"""

    cmd = ["sed"]
    cmd.append("-i")
    cmd.append("s/{}/{}/g".format(kwargs["string1"], kwargs["string2"]))
    cmd.append(kwargs["path"])
    # Do not write to stdout to keep output intact for piping.
    print(" ".join(cmd), file=sys.stderr)
    sys.exit(subprocess.call(cmd))


if __name__ == "__main__":
    execute()
