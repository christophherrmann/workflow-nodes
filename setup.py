# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os

from setuptools import find_packages
from setuptools import setup


with open(os.path.join("workflow_nodes", "version.py")) as f:
    exec(f.read())


with open("README.md") as f:
    long_description = f.read()


setup(
    name="workflow-nodes",
    version=__version__,
    author="Karlsruhe Institute of Technology",
    description="Collection of nodes for use in workflows.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=find_packages(),
    include_package_data=True,
    python_requires=">=3.6",
    zip_safe=False,
    classifiers=[],
    install_requires=[
        "click",
        "kadi-apy",
        "matplotlib",
        "openpyxl",
        "pandas",
        "pylatex",
        "xmlhelpy@https://gitlab.com/iam-cms/workflows/xmlhelpy/-/archive/master/xmlhelpy-master.tar.gz",
    ],
    extras_require={
        "dev": [
            "black==20.8b1",
            "pre-commit>=2.7.0",
            "pylint>=2.4.4,<2.5.0",
            "tox>=3.15.0",
        ]
    },
    entry_points={
        "console_scripts": [
            "StartReport = workflow_nodes.report.StartReport:execute",
            "EndReport = workflow_nodes.report.EndReport:execute",
            "ImageReport = workflow_nodes.report.ImageReport:execute",
            "TextReport = workflow_nodes.report.TextReport:execute",
            "InputReport = workflow_nodes.report.InputReport:execute",
            "PlotReport = workflow_nodes.report.PlotReport:execute",
            "MathReport = workflow_nodes.report.MathReport:execute",
            "CompileLatexReport = workflow_nodes.report.CompileLatexReport:execute",
            "AttachmentReport = workflow_nodes.report.AttachmentReport:execute",
            "system_awk = workflow_nodes.system.awk:execute",
            "system_bc = workflow_nodes.system.bc:execute",
            "system_paste = workflow_nodes.system.paste:execute",
            "system_sort = workflow_nodes.system.sort:execute",
            "system_mkdir = workflow_nodes.system.mkdir:execute",
            "system_sed = workflow_nodes.system.sed:execute",
            "system_cp = workflow_nodes.system.cp:execute",
            "CreateSymlink = workflow_nodes.misc.CreateSymlink:execute",
            "ImageJMacro = workflow_nodes.misc.ImageJMacro:execute",
            "ImageJVariable = workflow_nodes.misc.ImageJVariable:execute",
            "RunScript = workflow_nodes.misc.RunScript:execute",
            "RecordCreate = workflow_nodes.repo.RecordCreate:execute",
            "RecordAddFiles = workflow_nodes.repo.RecordAddFiles:execute",
            "RecordAddUser = workflow_nodes.repo.RecordAddUser:execute",
            "RecordAddMetadatum = workflow_nodes.repo.RecordAddMetadatum:execute",
            "RecordAddMetadata = workflow_nodes.repo.RecordAddMetadata:execute",
            "RecordRemoveUser = workflow_nodes.repo.RecordRemoveUser:execute",
            "RecordDelete = workflow_nodes.repo.RecordDelete:execute",
            "ExcelAddData = workflow_nodes.excel.ExcelAddData:execute",
            "ExcelData = workflow_nodes.excel.ExcelData:execute",
            "ExcelReadData = workflow_nodes.excel.ExcelReadData:execute",
            "DebugPrint = workflow_nodes.debug.DebugPrint:execute",
            "PlotVeusz = workflow_nodes.plot.PlotVeusz:execute",
            "PlotMatplotlib = workflow_nodes.plot.PlotMatplotlib:execute",
            "Pace2kadi = workflow_nodes.converter.Pace2kadi:execute",
            "Excel2kadi = workflow_nodes.converter.Excel2kadi:execute",
            "Kadi2kadi = workflow_nodes.converter.Kadi2kadi:execute",
            "LabVIEWCLI = workflow_nodes.wsl.LabVIEWCLI:execute",
            "system_echo = workflow_nodes.system.echo:execute",
            "Simulation_mpipace3D = workflow_nodes.simulation.mpipace3D:execute",
        ]
    },
)
